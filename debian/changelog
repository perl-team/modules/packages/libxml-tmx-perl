libxml-tmx-perl (0.39-2) unstable; urgency=medium

  * Team upload,
  * Remove generated test file via debian/clean. (Closes: #1047781)
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Mar 2024 11:48:23 +0100

libxml-tmx-perl (0.39-1) unstable; urgency=medium

  * Team upload,
  * Import upstream version 0.39.
  * Add new dependency: libutf8-all-perl.
  * Drop almost empty debian/upstream/metadata.
  * Declare compliance with Debian Policy 4.6.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.

 -- gregor herrmann <gregoa@debian.org>  Mon, 11 Jul 2022 11:30:11 +0200

libxml-tmx-perl (0.36-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 23:27:06 +0100

libxml-tmx-perl (0.36-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.

  [ Nuno Carvalho ]
  * Import upstream version 0.36.
  * debian/control:
    + add new dependency libfile-bom-perl.
    + declare compliance with Debian Policy 4.1.0.
    + drop build dependencies on libtest-pod*.
    + drop minimum version requirement for libxml-dt-perl, not needed.
  * debian/copyright:
    + update copyright years.
    + remove copyright stanzas no longer required.

 -- Nuno Carvalho <smash@cpan.org>  Thu, 07 Sep 2017 14:33:35 +0100

libxml-tmx-perl (0.31-1) unstable; urgency=medium

  * Team upload.
  * Add debian/upstream/metadata
  * Import upstream version 0.31
  * Update upstream copyright
  * Update debian/* copyright

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Sat, 16 Jan 2016 13:29:31 -0200

libxml-tmx-perl (0.29-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Update years of upstream copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sun, 01 Nov 2015 16:35:03 +0100

libxml-tmx-perl (0.25-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Nuno Carvalho ]
  * New upstream release
  * d/control: update standards version

 -- Nuno Carvalho <smash@cpan.org>  Fri, 02 Aug 2013 18:24:42 +0100

libxml-tmx-perl (0.22-1) unstable; urgency=low

  * New upstream release

 -- Nuno Carvalho <smash@cpan.org>  Thu, 07 Jun 2012 17:01:09 +0100

libxml-tmx-perl (0.21-1) unstable; urgency=low

  * Expand abbreviation in package description (Closes: #672645)
  * Remove patch to fix typos (fixed upstream)
  * Remove patch to install scripts as examples (fixed upstream)
  * New upstream release

 -- Nuno Carvalho <smash@cpan.org>  Wed, 06 Jun 2012 11:37:43 +0100

libxml-tmx-perl (0.20-1) unstable; urgency=low

  * Initial Release. (Closes: #670261)

 -- Nuno Carvalho <smash@cpan.org>  Tue, 24 Apr 2012 15:08:42 +0100
